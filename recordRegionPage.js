// Code for the Record Region page.

function addLocation(){
    let Location=document.getElementById("Locations").value
    let Date=document.getElementById("Date").value
    let Time=document.getElementById("Time").value
    let Moreinfo=document.getElementById("MoreInfo").value
    let outputMessageRef = document.getElementById("outputMessage")
    //make sure the input box is not empty
    if (Location === "" || Date === "" || Time === "" || Moreinfo === "" ){
        outputMessageRef.innerText = "Please fill in the empty boxes"   
    }else {
        //Opening index.HTML when location is saved
        function openIndex(){
            window.open("index.html","_self")
        }
        setTimeout(openIndex,4000)
        outputMessageRef.innerHTML = "The Location has been saved, you will be <b>redirected to the main page</b> shortly"
        let Region=new Region(Location,Date,Time,Moreinfo,"Available")
        let RegionList = JSON.parse(localStorage.getItem("RegionList"))
        
        let jsonRegionListInstance = JSON.stringify(RegionList)
        RegionList["Region"].push(Region)
        //Putting the new shipList into local storage
        localStorage.setItem("RegionList",jsonRegionListInstance)
    
}
}
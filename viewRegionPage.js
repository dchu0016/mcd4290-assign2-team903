// Code for the View Region page.

// The following is sample code to demonstrate navigation.
// You need not use it for final app.

var regionIndex = localStorage.getItem(APP_PREFIX + "-selectedRegion"); 
if (regionIndex !== null)
{
    // If a region index was specified, show name in header bar title. This
    // is just to demonstrate navigation.  You should set the page header bar
    // title to an appropriate description of the region being displayed.
    var regionNames = [ "Region A", "Region B" ];
    document.getElementById("headerBarTitle").textContent = regionNames[regionIndex];
}

let viewRegionTableRef=document.getElementById("viewRegionTable")
let RegionList=JSON.parse(localStorage.getItem("RegionList"))

for (let i=0;i<RegionList.Region.length;i++){
    let Location=RegionList.Region[i]._Location
    let Date=RegionList.Region[i]._Date
    let Time=RegionList.Region[i]._Time
    let Moreinfo=RegionList.Region[i]._Moreinfo
    let output=""
    output+="<tr class='tableRowStyle0'>"
    output += "<td class='mdl-data-table__cell--non-numeric'>" + Location +  "</td>"
    output += "<td class='mdl-data-table__cell--non-numeric'>" + Date +  "</td>"
    output += "<td class='mdl-data-table__cell--non-numeric'>" + Time +  "</td>"
    output += "<td class='mdl-data-table__cell--non-numeric'>" + Moreinfo +  "</td>"
    output +="</tr>"
    viewRegionTableRef.innerHTML=output
}
